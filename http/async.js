﻿var apiUrl = 'http://localhost:12111/webisida/dynamic-js/http/api.php';

print('Выполнение запроса');
var index = 0;
doQuery();

function doQuery() {
	switch(index++) {
		case 0:
			doGet();
			break;
		case 1:
			doGetJSON();
			break;
		case 2:
			doPost();
			break;
	}
}

function doGet() {
	http.get(apiUrl + '?type=text', function (data) {
		print('GET запрос успешно выполнен!');
		print(data);
	}).fail(function (data, status, error) {
		print('При выполнении запроса произошла ошибка: ' + error);
	}).always(doQuery);
}

function doGetJSON() {
	http.getJSON(apiUrl, function (data) {
		print('GET запрос успешно выполнен!');
		print('getJSON: ' +  data + (data ?  '. Hello, ' + data.hello : '.'));
	}).fail(function (data, status, error) {
		print('При выполнении запроса произошла ошибка: ' + error);
	}).always(doQuery);
}

function doPost() {
	http.post(apiUrl, { type: 'text' }, function (data) {
		print('POST запрос успешно выполнен!');
		print(data);
	}).fail(function (data, status, error) {
		print('При выполнении запроса произошла ошибка: ' + error);
	}).always(doQuery);
}