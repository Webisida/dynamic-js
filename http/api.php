<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');
header('Access-Control-Max-Age: 1000');
if(array_key_exists('HTTP_ACCESS_CONTROL_REQUEST_HEADERS', $_SERVER)) {
    header('Access-Control-Allow-Headers: '.$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']);
} else {
    header('Access-Control-Allow-Headers: *');
}

if("OPTIONS" == $_SERVER['REQUEST_METHOD']) {
    exit(0);
}

if(isset($_REQUEST['error'])) {
	header('HTTP/1.0 404 Not Found');
	die();
}

sleep(5);
$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : NULL;
switch($type) {
	case 'text':
		header('Content-Type: text/plain');
		break;
	default:
		header('Content-Type: application/json');
		break;
}
echo json_encode(array('hello' => 'world'));