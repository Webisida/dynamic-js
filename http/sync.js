﻿var apiUrl = 'http://localhost:12111/webisida/dynamic-js/http/api.php';

print('Выполнение запроса 1');
print('getSync: ' + http.getSync(apiUrl + '?type=text'));

print('Выполнение запроса 2');
print('postSync: ' + http.postSync(apiUrl, { type: 'text' }));

print('Выполнение запроса 3');
var data = http.getJSONSync(apiUrl);
print('getJSONSync: ' +  data + (data ?  '. Hello, ' + data.hello : '.'));


// Эти запросы должны вернуть undefined, т.к. api.php выдаст 404-ю ошибку.
print('Выполнение запроса 4');
print('getSync: ' + http.getSync(apiUrl + '?type=text&error=yes'));

print('Выполнение запроса 5');
print('postSync: ' + http.postSync(apiUrl, { type: 'text', error: 'yes' }));

print('Выполнение запроса 6');
var data = http.getJSONSync(apiUrl + '?error=yes');
print('getJSONSync: ' +  data + (data ?  '. Hello, ' + data.hello : '.'));