// 1. Получение письма, присланного с адреса noreply@youtube.com
// и содержащего текст 'Popular on YouTube' в теле или заголовке
var searchText = 'Popular on YouTube';
var email = getEmail('imap.gmail.com', 'example@gmail.com', 'password', {
	text: searchText,
	from: 'noreply@youtube.com'
});
if (email) {
	print('Письмо от ' + email.from + ' (' + email.contentType + '):')
	print(email);
	print('-----------------------------------------');
	print('Ссылки: ' + email.links);
	if(email.links && email.links.length > 0) {
		print('Ссылка #1: ' + email.links[0]);
	}
	if(email.alternateViews) {
		print('Количество альтернативных представлений: ' + email.alternateViews.length);
		for (var i = 0; i < email.alternateViews.length; i++) {
			var view = email.alternateViews[i];
			print('------------------------------------------');
			print('Вид #' + i + ' (' + view.contentType + '):');
			print(view.text);
		}
	}
} else {
	print('Нет письма, содержащего "' + searchText +'"');
}