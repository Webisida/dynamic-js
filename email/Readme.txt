﻿Тесты для функций получения содержимого писем.


1) email-1.js: Получение письма, содержащего указанный текст:
	getEmail(address, login, password, text)

2) email-2.js: Получение письма, содержащего указанный текст и присланного с указанного адреса:
	getEmail(address, login, password, params)

2) email-3.js: Получение письма, содержащего указанный текст, с указанием протокола соединения
               с почтовым сервером:
	getEmail(address, login, password, params)