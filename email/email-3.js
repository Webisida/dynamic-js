// 1. Получение по протоколу POP3 с использованием защищенного соединения
// 2-го по счету письма, содержащего в теле или заголовке
// текст 'Popular on YouTube', и последующее удаление этого письма
var searchText = 'Popular on YouTube';
var email = getEmail('mail.rambler.ru', 'example@rabmler.ru', 'password', {
	text: searchText,
	protocol: 'pop3', // указание протокола
	port: 995, // порт почтового сервера "Рамблер-Почты" для POP3 + SSL (отсюда http://help.rambler.ru/mail/mail-pochtovye-klienty/1275/)
	ssl: true, // т.к. POP3 + SSL
	index: 1, // для получения 2-го по счету письма из найденных 
	drop: true // для удаления письма
});
if (email) {
	print('Письмо от ' + email.from + ' (' + email.contentType + '):')
	print(email);
	print('-----------------------------------------');
	print('Ссылки: ' + email.links);
	if(email.links && email.links.length > 0) {
		print('Ссылка #1: ' + email.links[0]);
	}
	if(email.alternateViews) {
		print('Количество альтернативных представлений: ' + email.alternateViews.length);
		for (var i = 0; i < email.alternateViews.length; i++) {
			var view = email.alternateViews[i];
			print('------------------------------------------');
			print('Вид #' + i + ' (' + view.contentType + '):');
			print(view.text);
		}
	}
} else {
	print('Нет письма, содержащего "' + searchText +'"');
}