﻿navigate('http://forum.webisida.com/');
wait(5000);
var window = getWindow(0);
var $ = jQueryDef(window);
var image = $('img[src*=logo]').get(0);
var base64Image = screenshot(image, 'png');
if(base64Image) {
	print('Скриншот логотипа создан!');
}
if(base64Image == vars.screenshot) {
	print('Скриншот сохранен в переменные vars.screenshot и base64Image!');

	// создание копии в новой вкладке
	activateTab(1, function (win2) {
		win2.location.href = 'data:text/html,' + encodeURIComponent('<html><body></body></html>');
		wait(300);

		var img = win2.document.createElement('img');
		img.setAttribute('src', 'data:image/png;base64,' + base64Image);
		win2.document.body.appendChild(img);
		
	});
}