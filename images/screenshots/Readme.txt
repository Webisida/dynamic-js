﻿Тесты для функций создания скриншотов.


1) scan-1.js: создание скриншота указанной области (по координатам)
	screenshot(x, y, width, height, format, saveToDisk); // возвращает изображение в формате base64

2) scan-2.js: создание скриншота элемента картинки (img)
	screenshot(element, format, saveToDisk); // возвращает изображение в формате base64

3) scan-3.js: создание скриншота произвольного элемента
	screenshot(element, format, saveToDisk); // возвращает изображение в формате base64