﻿// Ключ сервиса Antigate.com, заменить на свой
var serviceKey = '%antigateKey%';

navigate('https://www.google.com/recaptcha/demo/');
wait(5000);
var window = getWindow(0);
var $ = jQueryDef(window);
var image = $('img[src*=recaptcha]').get(0); // поиск капчи
var rect = image.getBoundingClientRect(); // прямоугольник, ограничивающий капчу

// распознавание
recognize(rect.left, rect.top, rect.width, rect.height, {
	key: serviceKey,
	words: 2,
	timeout: 120000 // максимум 2 минуты (2 * 60 * 1000)
}, function () {
	// Если сервис распознал текст, vars.recognizedText содержит результат
	if (vars.recognizedText) {
		print('Текст на капче: ' + vars.recognizedText);
		setText($('input[name=recaptcha_response_field]').get(0), vars.recognizedText);
	} else {
		print('Текст не был распознан!');
	}
});