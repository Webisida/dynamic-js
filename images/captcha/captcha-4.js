﻿// Ключ сервиса Antigate.com, заменить на свой
var serviceKey = '%antigateKey%';

navigate('https://www.google.com/recaptcha/demo/');
wait(5000);
var window = getWindow(0);
var $ = jQueryDef(window);
var image = $('img[src*=recaptcha]').get(0); // поиск капчи

// распознавание с сохранением изображения на диск
print('Текст на капче: ' + recognize(image, { key: serviceKey, save: true }));
setText($('input[name=recaptcha_response_field]').get(0), vars.recognizedText);