﻿// Ключ сервиса Antigate.com, заменить на свой
var serviceKey = '%antigateKey%';

navigate('https://www.google.com/recaptcha/demo/');
wait(5000);
var window = getWindow(0);
var $ = jQueryDef(window);
var image = $('img[src*=recaptcha]').get(0); // поиск капчи
var rect = image.getBoundingClientRect(); // прямоугольник, ограничивающий капчу

// создание скриншота капчи и ее распознавание
print('Текст на капче: ' + recognize(rect.left, rect.top, rect.width, rect.height, serviceKey));
setText($('input[name=recaptcha_response_field]').get(0), vars.recognizedText);