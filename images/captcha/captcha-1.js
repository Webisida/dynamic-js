﻿// Ключ сервиса Antigate.com, заменить на свой
var serviceKey = '%antigateKey%';

navigate('https://www.google.com/recaptcha/demo/');
wait(5000);
var $ = jQueryDef(getWindow(0));
var image = $('img[src*=recaptcha]').get(0); // поиск капчи
if(image) {
	// распознавание
	print('Текст на капче: ' + recognize(image, serviceKey));
	setText($('input[name=recaptcha_response_field]').get(0), vars.recognizedText);
} else {
	print('Капча не найдена');
}
