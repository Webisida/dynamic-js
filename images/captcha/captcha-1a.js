﻿// Ключ сервиса Antigate.com, заменить на свой
var serviceKey = '%antigateKey%';

navigate('https://www.google.com/recaptcha/demo/');
wait(5000);
var $ = jQueryDef(getWindow(0));
var image = $('img[src*=recaptcha]').get(0); // поиск капчи

// распознавание
recognize(image, serviceKey, function () {
	// Если сервис распознал текст, vars.recognizedText содержит результат
	if (vars.recognizedText) {
		print('Текст на капче: ' + vars.recognizedText);
		setText($('input[name=recaptcha_response_field]').get(0), vars.recognizedText);
	} else {
		print('Текст не был распознан!');
	}
});