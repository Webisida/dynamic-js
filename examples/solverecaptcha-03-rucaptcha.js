﻿var rucaptchaKey = '...'; // ключ для rucaptcha.com

navigate('https://www.google.com/recaptcha/api2/demo');
wait(5000);
var captchaDiv = document.getElementById('recaptcha-demo');
if (captchaDiv != null) {
	var code = solveRecaptcha(captchaDiv, { service: 'rucaptcha', key: rucaptchaKey });
	if (code != null) {
		click('input', 'id', 'recaptcha-demo-submit');
	} else {
		print('Капча не распознана');
	}
} else {
	print('нет капчи');
}