﻿var rucaptchaKey = '...'; // ключ для сервиса ruCaptcha.com
 
// Узнаём ответ на вопрос "Какой сейчас год?".
recognize(null, {
	service: 'rucaptcha.com',
	comment: 'Какой сейчас год?',
	question: true,
	key: rucaptchaKey
})

print('Сейчас ' + vars.recognizedText + ' год.');

// остановка презентации
stop(true);