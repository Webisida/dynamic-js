﻿var antigateKey = ''; // ключ для antigate.com
var siteUrl = 'vk.com'; // адрес сайта
var ignoreAds = true; // не переходить по рекламе

// поисковые фразы
var searchKeys = [
	'заработок в интернете',
	'как заработать в сети',

];

// переход на Яндекс
navigate('http://www.yandex.ru');
wait(8, 12);

click('*', 'selector', '.modal__close');
pressKey(27); // ESC

// ввод поисковой фразы
typeIn('selector', 'input[name="text"]', searchKeys[random(searchKeys.length)]);
wait(2, 5);
// поиск...
click('button', 'text', 'Найти');
wait(8, 12);

// если вылезла капча
if (typeof antigateKey == 'string' && antigateKey.length == 32) {
	while (window.location.href.indexOf('/showcaptcha?') != -1) {
		// ставим галку 'Я не робот'
		click('*', 'selector', '.CheckboxCaptcha-Button');
		wait(5000);
		// распознаем капчу
		var img = document.querySelector('img[src*=captcha]');
		if (img) {
			var captchaText = null;
			while (!captchaText) {
				captchaText = recognize(img, { key: antigateKey, cyrillic: true });
				wait(3, 5);
			}
			setText('input', 'name=rep', captchaText);
			wait(100);
			submit('=LAST=', null);
		}
		wait(5, 7);
	}
}

click('*', 'selector', '.modal__close');
pressKey(27); // ESC

// Поиск ссылки
(function () {
	while (true) {
		let a = null;
		let links = document.getElementsByTagName('a');
		let len = links.length;
		for (var i = 0; i < len; i++) {
			a = links[i];
			if ((a.textContent || '').includes(siteUrl)) {
				var serpItem = a.closest('.serp-item,li');
				if (serpItem) {
					if (ignoreAds && (serpItem.innerHTML || '').includes('yabs.yandex'))
						continue;
					serpItem = serpItem.querySelector('h2');
				}
				click(serpItem || a);
				return;
			}
		}
		// Если сайта на странице нет, то переход на следующую страницу
		var next = document.querySelector('.pager__item_kind_next,.more__button');
		if (next != null) {
			click(next);
		} else {
			click('a', 'text', 'дальше');
		}
		wait(5, 10);
	}
})();

// нижерасположенный код выполняется на сайте
wait(25, 35); // загрузка страницы (точка входа на сайт)
// переходы до истечения времени
while(true) {
	loadSiteLink('link', '', -1);
	wait(15, 45);
}