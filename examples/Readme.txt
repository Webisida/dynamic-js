yandex.js - пример поиска в Яндексе
recaptcha-v2-rucaptcha.js - пример распознавания рекапчи версии 2.0 через ruCapctha.com
recaptcha-v2-antigate.js - пример распознавания рекапчи версии 2.0 через anti-captcha.com (antigate.com)
question-antigate.js - пример получения ответа на вопрос на английском языке через anti-captcha.com (antigate.com)
question-rucaptcha.js - пример получения ответа на вопрос на русском языке через ruCapctha.com
solverecaptcha-01-antigate.js - прохождение капчи "Я не робот" с помощью anti-captcha.com
solverecaptcha-02-antigate.js - прохождение капчи "Я не робот" путем отправки ключа на anti-captcha.com
solverecaptcha-03-antigate.js - прохождение капчи "Я не робот" с помощью anti-captcha.com
solverecaptcha-01-rucaptcha.js - прохождение капчи "Я не робот" с помощью rucapctha.com
solverecaptcha-02-rucaptcha.js - прохождение капчи "Я не робот" путем отправки ключа на rucapctha.com
solverecaptcha-03-rucaptcha.js - прохождение капчи "Я не робот" с помощью rucapctha.com
invisible-recaptcha-antigate.js - прохождение невидимой Google-капчи с помощью anti-captcha.com
invisible-recaptcha-rucaptcha.js - прохождение невидимой Google-капчи с помощью rucapctha.com
