﻿var antigateKey = '...'; // ключ для anti-captcha.com (antigate.com)
 
// Узнаём ответ на вопрос "What color is the sky?".
recognize(null, {
	comment: 'What color is the sky?',
	question: true,
	key: antigateKey
})

print("The sky's color is " + vars.recognizedText);

// остановка презентации
stop(true);