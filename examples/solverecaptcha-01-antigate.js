﻿var antigateKey = '...'; // ключ для anti-captcha.com

navigate('https://www.google.com/recaptcha/api2/demo');
wait(5000);
var code = solveRecaptcha(null, antigateKey);
if (code != null) {
	click('input', 'id', 'recaptcha-demo-submit');
} else {
	print('Капча не распознана');
}