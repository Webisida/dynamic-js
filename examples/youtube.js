﻿var videoLinkPart = 'zsKbfS6-lSI'; // часть из ссылки на видео http://www.youtube.com/watch?v=zsKbfS6-lSI
var searchQuery = 'улыбка'; // поисковый запрос


// вход на YouTube
navigate('http://www.youtube.com/');
wait(15, 25);
// ввод в поисковую строку
setText('input', 'name=search_query', searchQuery);
wait(3, 5);
click('button', 'custom', 'type=submit');
wait(15, 25);

// поиск видео по части ссылки
var videoLink = null;
while((videoLink = document.querySelector('a[href*="' + escapeSelectorData(videoLinkPart) + '"]')) == null) {
	click('a', 'selector', 'a[data-link-type=next]'); // переход на следующую страницу
	wait(10, 15);
}
click(videoLink); // клик по ссылке на видео