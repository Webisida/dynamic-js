﻿var rucaptchaKey = '...'; // ключ для rucaptcha.com

navigate('https://www.google.com/recaptcha/api2/demo');
wait(5000);
var code = solveRecaptcha(null, { service: 'rucaptcha', key: rucaptchaKey, timeout: 60000 });
if (code != null) {
	click('input', 'id', 'recaptcha-demo-submit');
} else {
	print('Капча не распознана');
}