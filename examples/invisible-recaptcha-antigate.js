﻿var antigateKey = '...'; // ключ для anti-captcha.com

navigate('https://www.google.com/recaptcha/api2/demo?invisible=true');
wait(5000);
var code = solveRecaptcha(null, antigateKey);
if (code != null) {

	// Вызов функции, которая выполняется после прохождения капчи.
	// Ссылка на неё находится внутри одного из полей переменной ___grecaptcha_cfg.clients
	// (нужно искать поле callback, например, так: ___grecaptcha_cfg.clients[0].W.Vk.callback).
	// Поле callback может быть ссылкой на функцию или строкой (в данном случае это строка onSuccess).
	
	// Следующий код пытается найти эту callback-функцию ___grecaptcha_cfg.clients[0] и вызвать её.
	executeJavaScript("(function(k,c,f,z) { c = ___grecaptcha_cfg.clients[0]; for(var n in c) { z = c[n]; if((z+'')[8] == 'O') { for(var t in z) if(z[t] && (f = z[t].callback)) { if(f.call) f(k); else window[f](k); return } } } })('" + code + "')");

} else {
	print('Капча не пройдена');
}