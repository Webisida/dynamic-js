﻿var antigateKey = '...'; // ключ для anti-captcha.com

navigate('https://www.google.com/recaptcha/api2/demo');
wait(5000);
var code = solveRecaptcha(null, {
	key: antigateKey,
	siteUrl: 'www.google.com',
	siteKey: '6Le-wvkSAAAAAPBMRTvw0Q4Muexq9bi0DJwx_mJ-',
	timeout: 60000 // 60 секунд.
});
if (code != null) {
	click('input', 'id', 'recaptcha-demo-submit');
} else {
	print('Капча не распознана');
}